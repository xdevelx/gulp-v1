const config = {
  build: {
    dir: 'build/',
    files: 'build/**/*'
  },
  styles: {
    dir: 'src/scss/',
    src: 'src/scss/style.scss',
    dest: 'build/css/',
    output: 'build/css/style.css',
    watch: 'src/scss/**/*.scss'
  },
  scripts: {
    dir: 'src/js/',
    src: 'src/js/main.js',
    dest: 'build/js/',
    output: 'build/js/app.js',
    watch: 'src/js/**/*.js'
  },
  fonts: {
    dir: 'src/fonts/',
    src: 'src/fonts/**/*.{woff,woff2,ttf,eot}',
    dest: 'build/fonts/'
  },
  images: {
    dir: 'src/images/',
    src: 'src/images/*.{png,jpg,jpeg,gif,svg}',
    dest: 'build/images/',
    svg_symbol_icons: 'src/images/svg-symbol-icons/*.svg',
    svg_sprite_icons: 'src/images/svg-sprite-icons/*.svg',
    png_icons: 'src/images/png-icons/*.png'
  },
  views: {
    dir: 'src/views/',
    src: 'src/views/pages/*.njk',
    dest: 'build/',
    watch: 'src/views/**/*.njk',
    inline: 'src/views/inline'
  },
  deploy: {
    host: 'gsmrepairco@195.191.24.138',
    dest: 'home/gsmrepairco/public_html/temp'
  }
};

export default config;
