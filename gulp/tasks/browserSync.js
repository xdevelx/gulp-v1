import { watch } from 'gulp';
import browserSync from 'browser-sync';
import styles from './styles';
import scripts from './scripts';
import nunjucks from './nunjucks';
import config from '../config';

export const bs = browserSync.create();

export function server() {
  bs.init({
    notify: true,
    server: config.build.dir
  });

  watch(config.styles.watch, styles);
  watch(config.scripts.watch, scripts);
  watch(config.views.watch, nunjucks);
}
