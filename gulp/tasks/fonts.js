import { dest, src} from 'gulp';
import config from '../config';

export default function fonts() {
  return src(config.fonts.src)
    .pipe(dest(config.fonts.dest));
}
