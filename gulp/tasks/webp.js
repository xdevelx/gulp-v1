import { dest, src} from 'gulp';
import cache from 'gulp-cache';
import imagemin from 'gulp-imagemin';
import imageminWebp from 'imagemin-webp';
import rename from 'gulp-rename';
import config from '../config';

// Images optimization
export default function webp() {
  return src('src/images/*.{png,jpg,jpeg}')
    .pipe(cache(imagemin([imageminWebp({quality: '70'})], {verbose: true})))
    .pipe(rename((path) => {
      path.extname = '.webp';
    }))
    .pipe(dest(config.images.dest));
}
