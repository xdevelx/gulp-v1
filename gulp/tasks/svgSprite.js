import { dest, src} from 'gulp';
import rename from 'gulp-rename';
import cheerio from 'gulp-cheerio';
import plumber from 'gulp-plumber';
import imagemin from 'gulp-imagemin';
import sprite from 'gulp-svg-sprite';
import config from '../config';

function svgSymbols() {
  return src(config.images.svg_symbol_icons)
    .pipe(plumber())
  //  .pipe(imagemin([imagemin.svgo()]))
    .pipe(cheerio({
      run($) {
        $('[fill]').not('[fill="currentColor"]').removeAttr('fill');
        $('[stroke]').removeAttr('stroke');
        $('[style]').removeAttr('style');
        $('[data-original]').removeAttr('data-original');
        $('[data-old_color]').removeAttr('data-old_color');
        $('style').remove();
      },
      parserOptions: {xmlMode: true}
    }))
    .pipe(rename({prefix: 'icon-'}))
    .pipe(sprite({
      mode: {
        symbol: {
          inline: true,
          sprite: '../symbols.svg'
        }
      }
    }))
    .pipe(dest(config.images.dest))
    .pipe(dest(config.views.inline));
}

function svgSprite() {
  return src(config.images.svg_sprite_icons)
    .pipe(plumber())
    .pipe(imagemin([imagemin.svgo()]))
    .pipe(sprite({
      mode: {
        css: {
          dest: '../images',
          bust: true,
          sprite: 'sprite.svg',
          render: {
            scss: {
              dest: '../../' + config.styles.dir + '_svg.sprite.scss',
              template: config.styles.dir + 'sprite-templates/svg-sprite.template.mustache'
            }
          }
        }
      }
    }))
    .pipe(dest(config.images.dest));
}

export { svgSymbols, svgSprite };
