import env from 'gulp-env';

export function setDev(cb) {
  env.set({NODE_ENV: 'dev'});
  cb();
}
export function setProd(cb) {
  env.set({NODE_ENV: 'production'});
  cb();
}
