import { dest, src} from 'gulp';
import gulpIf from 'gulp-if';
import plumber from 'gulp-plumber';
import uglify from 'gulp-uglify';
import webpack from 'webpack-stream';
import { bs } from './browserSync';
import config from '../config';

export default function scripts() {
  const isDev = !process.env.NODE_ENV || process.env.NODE_ENV === 'dev';
  return src(config.scripts.src)
    .pipe(plumber())
    .pipe(webpack({
      output: {filename: 'app.js'},
      mode: 'none',
      module: {
        rules: [{
          test: /\.(js)$/,
          exclude: /(node_modules)/,
          loader: 'babel-loader',
          query: {presets: ['@babel/preset-env']}
        }]
      }
    }))
    .pipe(uglify())
    .pipe(dest(config.scripts.dest))
    .pipe(gulpIf(isDev, bs.stream()));
}
