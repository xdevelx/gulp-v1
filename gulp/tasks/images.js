import { dest, src } from 'gulp';
import cache from 'gulp-cache';
import imagemin from 'gulp-imagemin';
import pngquant from 'imagemin-pngquant';
import mozjpeg from 'imagemin-mozjpeg';
import imageminJpegRecompress from 'imagemin-jpeg-recompress';
import config from '../config';

// Images optimization
export default function images() {
  return src(config.images.src)
    .pipe(cache(imagemin(
      [
        pngquant({
          quality: '60',
          speed: 5
        }),
        mozjpeg({quality: 90}),
        imageminJpegRecompress({
          loops: 5,
          min: 65,
          max: 70,
          quality: 'medium' // Available presets: low, medium, high and veryhigh.
        }),
        imagemin.svgo(),
        imagemin.gifsicle({interlaced: true})
      ],
      {verbose: true}
    )))
    .pipe(dest(config.images.dest));
}
