import { dest, src} from 'gulp';
import merge from 'merge-stream';
import buffer from 'vinyl-buffer';
import hydra from 'gulp-hydra';
import spritesmith from 'gulp.spritesmith';
import spritesmash from 'gulp-spritesmash';
import cache from 'gulp-cache';
import imagemin from 'gulp-imagemin';
import pngquant from 'imagemin-pngquant';
import config from '../config';

export default function pngSprite() {
  // Generate spritesheet
  const spriteData = src(config.images.png_icons)
    .pipe(spritesmith({
      imgName: 'sprite.png',
      cssName: '_png.sprite.scss',
      cssTemplate: config.styles.dir + 'sprite-templates/png-sprite.template.mustache',
      imgPath: '../images/sprite.png'
    }))
    .pipe(buffer())
    .pipe(spritesmash())
    .pipe(hydra({
      img: {
        type: 'ext',
        filter: ['.png', '.jpg']
      },
      css: {
        type: 'ext',
        filter: '.scss'
      }
    }));

  // Pipe image stream through image optimizer and onto disk
  const imgStream = spriteData.img
    // DEV: We must buffer our stream into a Buffer for `imagemin`
    .pipe(buffer())
    .pipe(cache(imagemin(
      [pngquant({
        quality: '60',
        speed: 5
      })],
      {verbose: true}
    )))
    .pipe(dest(config.images.dest));

  // Pipe CSS stream through CSS optimizer and onto disk
  const cssStream = spriteData.css.pipe(dest(config.styles.dir));

  // Return a merged stream to handle both `end` events
  return merge(imgStream, cssStream);
}
