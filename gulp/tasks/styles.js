import { dest, src} from 'gulp';
import gulpIf from 'gulp-if';
import plumber from 'gulp-plumber';
import postcss from 'gulp-postcss';
import autoprefixer from 'autoprefixer';
import sass from 'gulp-sass';
import csso from 'gulp-csso';
import glob from 'gulp-sass-glob';
import lost from 'lost';
import { bs } from './browserSync';
import config from '../config';

export default function styles() {
  const isDev = !process.env.NODE_ENV || process.env.NODE_ENV === 'dev';
  return src(config.styles.src, {sourcemaps: isDev})
    .pipe(plumber())
    .pipe(glob())
    .pipe(sass({
      includePaths: ['node_modules/'],
      outputStyle: 'expanded'
    }))
    .pipe(postcss([autoprefixer(), lost()]))
    .pipe(gulpIf(!isDev, csso({comments: false})))
    .pipe(dest(config.styles.dest, {sourcemaps: isDev}))
    .pipe(gulpIf(isDev, bs.stream()));
}
