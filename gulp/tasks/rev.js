import { dest, src} from 'gulp';
import rev from 'gulp-rev';
import revdel from 'gulp-rev-delete-original';
import config from '../config';

export default function revisions() {
  return src([config.styles.output, config.scripts.output], {base: config.build.dir})
    .pipe(rev())
    .pipe(revdel())
    .pipe(dest(config.build.dir))
    .pipe(rev.manifest({merge: true}))
    .pipe(dest(config.build.dir));
}
