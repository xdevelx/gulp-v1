import { dest, src } from 'gulp';
import gulpIf from 'gulp-if';
import plumber from 'gulp-plumber';
import njk from 'gulp-nunjucks-render';
import data from 'gulp-data';
import htmlmin from 'gulp-htmlmin';
import fs from 'fs';
import { bs } from './browserSync';
import config from '../config';

export default function nunjucks() {
  const isDev = !process.env.NODE_ENV || process.env.NODE_ENV === 'dev';
  const dataObject = {
    dev: isDev,
    css: 'css/style.css',
    js: 'js/app.js'
  };

  if (!isDev) {
    const manifest = JSON.parse(fs.readFileSync(config.build.dir + 'rev-manifest.json'));
    dataObject.css = manifest['css/style.css'];
    dataObject.js = manifest['js/app.js'];
  }

  return src(config.views.src)
    .pipe(plumber())
    .pipe(data(() => dataObject))
    .pipe(njk())
    .pipe(gulpIf(!isDev, htmlmin({collapseWhitespace: true})))
    .pipe(dest(config.views.dest))
    .pipe(gulpIf(isDev, bs.stream()));
}
