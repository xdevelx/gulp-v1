import { src } from 'gulp';
import gulpRsync from 'gulp-rsync';
import config from '../config';

export default function rsync() {
  return src(config.build.files)
    .pipe(gulpRsync({
      root: config.build.dir,
      hostname: config.deploy.host,
      destination: config.deploy.dest,
      archive: true,
      silent: false,
      progress: true,
      compress: true
    }));
}
