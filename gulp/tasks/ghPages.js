import { src } from 'gulp';
import ghPages from 'gulp-gh-pages';
import moment from 'moment';
import config from '../config';

const buildDate = moment().format('YYYY-MM-DD H:mm');

export default function githubPages() {
  return src(config.build.files)
    .pipe(ghPages({
      force: true,
      message: 'Build ' + buildDate
    }));
}
