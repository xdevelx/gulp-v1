import { parallel, series } from 'gulp';
import styles from './gulp/tasks/styles';
import scripts from './gulp/tasks/scripts';
import nunjucks from './gulp/tasks/nunjucks';
import clean from './gulp/tasks/clean';
import clearCache from './gulp/tasks/clearCache';
import fonts from './gulp/tasks/fonts';
import images from './gulp/tasks/images';
import pngSprite from './gulp/tasks/pngSprite';
import webp from './gulp/tasks/webp';
import githubPages from './gulp/tasks/ghPages';
import revisions from './gulp/tasks/rev';
import { server } from './gulp/tasks/browserSync';
import { svgSymbols, svgSprite } from './gulp/tasks/svgSprite';
import { setDev, setProd } from './gulp/tasks/setEnv';

// Export only used individual tasks from gulp/tasks directory
export { clearCache, clean, scripts, styles };

const imagesTasks = parallel(images, svgSymbols, svgSprite, pngSprite, webp);

export const buildDev = series(
  parallel(clean, setDev),
  parallel(fonts, imagesTasks, scripts, styles, nunjucks)
);
export const buildProd = series(
  parallel(clean, setProd),
  parallel(fonts, imagesTasks, series(parallel(scripts, styles), revisions, nunjucks))
);
export const dev = series(buildDev, server);
export const deploy = series(buildProd, githubPages);

// Export a default task
export default dev;
